package com.example.montagarfa.tunevent.network;

import com.example.montagarfa.tunevent.BuildConfig;
import com.example.montagarfa.tunevent.dataServices.BookingWebService;
import com.example.montagarfa.tunevent.dataServices.CategoryWebService;
import com.example.montagarfa.tunevent.dataServices.EventWebService;
import com.example.montagarfa.tunevent.dataServices.UserWebService;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {
    public static final int CONNECT_TIMEOUT_IN_MS = 30000;
    private static final String BASE_URL = "https://bc62a73b.ngrok.io/";

    @Provides
    @Singleton
    Interceptor requestInterceptor(RequestInterceptor interceptor) {
        return interceptor;
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(RequestInterceptor requestInterceptor) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(CONNECT_TIMEOUT_IN_MS, TimeUnit.MILLISECONDS)
                .addInterceptor(requestInterceptor);
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(loggingInterceptor);
        }

        return builder.build();
    }

    @Provides
    @Singleton
    Retrofit retrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    BookingWebService bookingWebService(Retrofit retrofit) {
        return retrofit.create(BookingWebService.class);
    }

    @Provides
    @Singleton
    CategoryWebService categoryWebService(Retrofit retrofit) {
        return retrofit.create(CategoryWebService.class);
    }

    @Provides
    @Singleton
    EventWebService eventWebService(Retrofit retrofit) {
        return retrofit.create(EventWebService.class);
    }

    @Provides
    @Singleton
    UserWebService userWebService(Retrofit retrofit) {
        return retrofit.create(UserWebService.class);
    }
}
