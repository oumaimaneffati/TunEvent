package com.example.montagarfa.tunevent.dataServices;

import com.example.montagarfa.tunevent.model.Event;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface EventWebService {
    @Headers("Content-Type: application/json")
    @GET("api/event/all")
    Call<List<Event>> getAllEvents();

    @GET("api/event/{id}")
    Call<Event> getEventById(@Path("id") String id);

    @POST("api/event")
    Event addEvent(@Body Event event);

    @DELETE("api/event/{id}")
    void deleteEventById(@Path("id") String id);
}
