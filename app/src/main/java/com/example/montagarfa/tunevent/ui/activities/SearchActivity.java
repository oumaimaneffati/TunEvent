package com.example.montagarfa.tunevent.ui.activities;

import android.os.Bundle;
import android.util.Log;

import com.example.montagarfa.tunevent.R;
import com.example.montagarfa.tunevent.model.Event;
import com.example.montagarfa.tunevent.ui.recyclerView.HomeRecyclerViewAdapter;
import com.example.montagarfa.tunevent.ui.recyclerView.SearchRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class SearchActivity extends AppCompatActivity {
    private static final String TAG = "SearchActivity";
    ArrayList<Event> events= new ArrayList<Event>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Log.d(TAG, "onCreate: started.");
        initListEvent();
    }

    private void initListEvent(){
        Log.d(TAG,"ini list event: preparing .") ;
        Event event = new Event("Concert","Tunisie",new Date(),Long.valueOf(20),Long.valueOf(20),null,null,"https://c1.staticflickr.com/5/4636/25316407448_de5fbf183d_o.jpg");
        for (int i =0;i<10;i++){
            events.add(event);
        }
        initRecyledView();
    }

    private void initRecyledView(){
        System.out.println("garfa montassar");
        Log.d(TAG,""+events.size());
        Log.d(TAG, "initRecyclerView: init recyclerview.");
        RecyclerView recyclerView = findViewById(R.id.recylce_view_searcj);
        // RecycleViewAdapter adapter = new RecycleViewAdapter(this, mNames, mImageUrls);
        //  SearchRecyclerViewAdapter adapter = new SearchRecyclerViewAdapter(this,listEvent);
        SearchRecyclerViewAdapter adapter1 = new SearchRecyclerViewAdapter(this,events);
        recyclerView.setAdapter(adapter1);
        System.out.println("garfa monta");
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

}
