package com.example.montagarfa.tunevent.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.example.montagarfa.tunevent.R;
import com.example.montagarfa.tunevent.ui.activities.SearchActivity;
import com.example.montagarfa.tunevent.ui.activities.SignupActivity;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchFragment extends Fragment {
    private static final int REQUEST_SIGNUP = 0;
    private static final String TAG = "SearchFragment";
    @BindView(R.id.search_date)
    AppCompatSpinner search_date;
    @BindView(R.id.search_location)
    AppCompatSpinner search_location;
    @BindView(R.id.search_category)
    AppCompatSpinner search_category;
    @BindView(R.id.btn_search)
    Button btnsearch;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search,null);

        ButterKnife.bind(this,view);
        initListDate();
        initListLocation();
        initListSearch();
        btnsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search();
            }
        });
        return view;
    }
    public void search(){
        Log.d(TAG, "onClick: signup");

        Intent intent = new Intent(getContext(), SearchActivity.class);
        startActivityForResult(intent, REQUEST_SIGNUP);
        getActivity().finish();
        getActivity().overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
    public void initListDate(){
        ArrayList<String> arrayDate =  new ArrayList<String>();
        arrayDate.add("ALL");
        arrayDate.add("Today");
        arrayDate.add("Tomorrow");
        arrayDate.add("This week");
        arrayDate.add("This mouth");
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getContext(),R.layout.spinner_item,arrayDate);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        search_date.setAdapter(spinnerArrayAdapter);
    }
    public void initListLocation(){
        ArrayList<String> arrayDate =  new ArrayList<String>();
        arrayDate.add("ALL");
        arrayDate.add("Today");
        arrayDate.add("Tomorrow");
        arrayDate.add("This week");
        arrayDate.add("This mouth");
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getContext(),R.layout.spinner_item,arrayDate);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        search_location.setAdapter(spinnerArrayAdapter);

    }
    public void initListSearch(){
        ArrayList<String> arrayDate =  new ArrayList<String>();
        arrayDate.add("ALL");
        arrayDate.add("Today");
        arrayDate.add("Tomorrow");
        arrayDate.add("This week");
        arrayDate.add("This mouth");
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getContext(),R.layout.spinner_item,arrayDate);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        search_category.setAdapter(spinnerArrayAdapter);
    }

}
