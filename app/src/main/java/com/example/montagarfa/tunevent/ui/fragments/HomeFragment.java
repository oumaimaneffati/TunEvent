package com.example.montagarfa.tunevent.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.montagarfa.tunevent.R;
import com.example.montagarfa.tunevent.model.Event;
import com.example.montagarfa.tunevent.ui.recyclerView.HomeRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.Date;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class HomeFragment extends Fragment {
    View view;
    private static String TAG="Home Fragment  ";

    RecyclerView recyclerView ;

    // list of events
    private ArrayList<Event> listEvent=new ArrayList<Event>();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         view =  inflater.inflate(R.layout.fragment_home,null);
      //  initRecyledView(view);
        return view;
    }

    private void initListEvents(View view){
        Log.d(TAG,"ini list event: preparing .") ;
        Event event = new Event("Concert","Tunisie",new Date(),Long.valueOf(20),Long.valueOf(20),null,null,"https://c1.staticflickr.com/5/4636/25316407448_de5fbf183d_o.jpg");
        for (int i =0;i<10;i++){
            listEvent.add(event);
        }
        initRecyledView(view);
    }
    private void initRecyledView(View view){
        System.out.println("garfa montassar");
        Log.d(TAG,""+listEvent.size());
        Log.d(TAG, "initRecyclerView: init recyclerview.");
        // RecycleViewAdapter adapter = new RecycleViewAdapter(this, mNames, mImageUrls);
        //  SearchRecyclerViewAdapter adapter = new SearchRecyclerViewAdapter(this,listEvent);
        recyclerView = (RecyclerView) view.findViewById(R.id.recylce_view_home);

        HomeRecyclerViewAdapter adapter1 = new HomeRecyclerViewAdapter(getContext(),listEvent);
        System.out.println("garfa monta");
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter1);

    }


}
