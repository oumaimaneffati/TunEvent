package com.example.montagarfa.tunevent.ui.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.montagarfa.tunevent.R;
import com.example.montagarfa.tunevent.ui.activities.SignupActivity;
import com.google.android.material.textfield.TextInputEditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileFragment extends Fragment {
    private static final int REQUEST_SIGNUP = 0;

    private static final String TAG = "ProfileFragment";
    @BindView(R.id.input_email)
    EditText email;
    @BindView(R.id.input_password)
    EditText password;
    @BindView(R.id.btn_login)
    AppCompatButton login;
    @BindView(R.id.link_signup)
    TextView signup;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: load");
        View view = inflater.inflate(R.layout.fragment_profile,null);
        ButterKnife.bind(this,view);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: logn");
                login();
            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: signup");

                Intent intent = new Intent(getContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
        return view;
    }

    // login pressed
    public void login(){
        if(!validate()){
            onLoginFailed();
            return;
        }

        final ProgressDialog progressDialog = new ProgressDialog(getActivity(),
                R.color.colorTI);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        String emailV= email.getText().toString();
        String passwordV= password.getText().toString();
        // TODO: 11/12/18

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        onLoginSuccess();
                        //monta.garfa@gmail.com onLoginFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);
    }

    //validate mail password
    public boolean validate(){
        boolean valid = true;
        String emailV = email.getText().toString();
        String passwordV = password.getText().toString();
        if (emailV.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(emailV).matches()){
            email.setError("enter a valid email address");
            valid = false;
        }
        if (passwordV.isEmpty() || passwordV.length()<4){
            password.setError("enter a valid password");
            valid=false;
        }
        return valid;
    }

    // login failed
    public void onLoginFailed() {
        Toast.makeText(getContext(), "Login failed", Toast.LENGTH_LONG).show();

        login.setEnabled(true);
    }
    //login success
    public void onLoginSuccess() {
        login.setEnabled(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
